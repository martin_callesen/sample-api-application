package dk.samples;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/** startup of spring container in an embedded tomcat server **/
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class ApiApplicationTest {
    private static final String LOCALHOST = "http://localhost:";

    @Value("${local.server.port}")
    private int serverPort;
    private RestTemplate restTemplate = new TestRestTemplate();

    protected <RESP> ResponseEntity<RESP> getForEntity(String serviceUrl, Class aClass) {
        String url = appendUrlRoot(serviceUrl);

        return this.restTemplate.getForEntity(url, aClass);
    }

    protected <RESP> ResponseEntity<RESP> getForEntity(String serviceUrl, Class aClass, Object... urlParams) {
        String url = appendUrlRoot(serviceUrl);

        return this.restTemplate.getForEntity(url, aClass, urlParams);
    }

    public <RESP> ResponseEntity<RESP> postForEntity(String serviceUrl, Object request, Class entityClass) {
        String url = appendUrlRoot(serviceUrl);
        Map<String, String> urlParams = new HashMap<>();

        return this.restTemplate.postForEntity(url, request, entityClass, urlParams);
    }

    private String appendUrlRoot(String serviceUrl) {
        return LOCALHOST + serverPort + serviceUrl;
    }
}
