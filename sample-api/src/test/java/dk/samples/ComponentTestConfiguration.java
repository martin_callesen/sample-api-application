package dk.samples;

import dk.samples.database.DatabaseMigrator;
import dk.samples.external.client.ExternalQuoteClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Configuration
@Profile("ci")
public class ComponentTestConfiguration {
    @Bean
    public ExternalQuoteClient externalQuoteClient() {
        return mock(ExternalQuoteClient.class);
    }

    @Bean
    public DatabaseMigrator databaseMigrator() {
        return mock(DatabaseMigrator.class);
    }
}
