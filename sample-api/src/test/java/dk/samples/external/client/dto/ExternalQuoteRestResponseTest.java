package dk.samples.external.client.dto;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;

public class ExternalQuoteRestResponseTest {
    @Test
    public void noResponse() throws Exception {
        ExternalQuoteRestResponse response = ExternalQuoteRestResponse.NO_QUOTE_AVAILABLE;
        assertThat("type", response.getType(), is(ExternalQuoteRestResponse.NO_QUOTE));
        assertThat("value", response.getValue(), is(ExternalQuoteRestResponse.NO_VALUE));
    }

    @Test
    public void responseWithAQuote() throws Exception {
        String quote = "A quote for testing";
        ExternalQuoteRestResponse response = ExternalQuoteRestResponse.withQuote(quote, ExternalQuoteRestResponse.QUOTE_AVAILABLE);
        assertThat("quote", response.toString(), containsString(quote));
    }
}