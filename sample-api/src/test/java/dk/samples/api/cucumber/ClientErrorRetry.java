package dk.samples.api.cucumber;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CountDownLatch;

class ClientErrorRetry extends RestTemplate {
    private CountDownLatch countDownLatch = new CountDownLatch(60);

    public ClientErrorRetry() {
        super();
    }

    @Override
    public <T> ResponseEntity<T> postForEntity(String url, Object request, Class<T> responseType, Object... uriVariables) throws RestClientException {
        ResponseEntity<T> entity;

        try {
            entity = super.postForEntity(url, request, responseType, uriVariables);
        } catch (Exception e){
            wait500(url);

            return postForEntity(url, request, responseType);
        }

        return entity;
    }

    @Override
    public <T> ResponseEntity<T> getForEntity(String url, Class<T> responseType, Object... urlVariables) throws RestClientException {
        ResponseEntity<T> entity;

        try {
            entity = super.getForEntity(url, responseType, urlVariables);
        } catch (Exception rcre) {
            wait500(url);

            return getForEntity(url, responseType, urlVariables);
        }

        return entity;
    }

    private void wait500(String url) {
        countDownLatch.countDown();

        if(countDownLatch.getCount() == 0){
            throw new RuntimeException("Server at url: "+url+" not available");
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
