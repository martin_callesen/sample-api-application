package dk.samples.api.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.samples.api.dto.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static dk.samples.api.service.QuoteLogRestService.QUOTE_LOG_REQUEST_URL;
import static dk.samples.api.service.QuoteRestService.QUOTE_REQUEST_URL;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class QuoteStepdefs {
    private static final String ROOT = "http://sample-api:8080";
    private ResponseEntity<QuoteRestResponse> quoteRestResponse;
    private String externalQuote;
    private ResponseEntity<QuoteLogSearchRestResponse> searchRestResponse;

    @Given("^quote \"([^\"]*)\"$")
    public void quote(String externalQuote) throws Throwable {
        this.externalQuote = externalQuote;
    }

    @Given("^quote retrieved \"([^\"]*)\"$")
    public void quote_viewed(String period){
        QuoteLogRestRequest request = QuoteLogRestRequest.create(externalQuote);
        RestTemplate restTemplate = new ClientErrorRetry();
        restTemplate.postForEntity(ROOT + QUOTE_LOG_REQUEST_URL, request, QuoteLogRestResponse.class);
    }

    @When("^I retrieve a quote$")
    public void quote_has_been_viewed () throws Throwable {
        RestTemplate restTemplate = new ClientErrorRetry();
        this.quoteRestResponse = restTemplate.getForEntity(ROOT+ QUOTE_REQUEST_URL, QuoteRestResponse.class);
        assertNotNull("QuoteRestResponse not received", this.quoteRestResponse);
    }

    @When("^I retrieve quotes viewed \"([^\"]*)\"$")
    public void i_retrieve_quotes_viewed(String period) throws Throwable {
        QuoteLogSearchRestRequest request = QuoteLogSearchRestRequest.valueOf(period.toUpperCase());
        Class entityClass = QuoteLogSearchRestResponse.class;
        int periodFromEnum = request.getPeriodeType();

        RestTemplate restTemplate = new ClientErrorRetry();
        String url = ROOT + QUOTE_LOG_REQUEST_URL + "?period={period}";
        this.searchRestResponse = restTemplate.getForEntity(url, entityClass, periodFromEnum);
        assertNotNull("QuoteLogSearchRestResponse not received", this.searchRestResponse);
    }

    @Then("^quote should be \"([^\"]*)\"$")
    public void quote_should_be(String expectedQuote) throws Throwable {
        assertNotNull("QuoteRestResponse not received", this.quoteRestResponse);
        QuoteRestResponse response = this.quoteRestResponse.getBody();
        String actualQuote = response.getQuote();
        assertThat("Response", actualQuote, equalTo(expectedQuote));
    }

    @Then("^I can see a quote has been viewed \"([^\"]*)\"$")
    public void i_can_see_a_quote_has_been_viewed(String period) throws Throwable {
        assertNotNull("QuoteLogSearchRestResponse not received", this.searchRestResponse);
        QuoteLogSearchRestResponse response = this.searchRestResponse.getBody();
        assertTrue("QuoteLogSearchRestResponse", response.containsQuote(this.externalQuote));
    }

}
