package dk.samples.api.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "html:target/cucumber" }, features = "classpath:dk/samples/api/cucumber/quote.feature")
public class QuoteSystemTest {
}
