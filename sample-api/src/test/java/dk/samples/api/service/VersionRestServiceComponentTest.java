package dk.samples.api.service;

import dk.samples.ApiApplication;
import dk.samples.ApiApplicationTest;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static dk.samples.api.service.VersionRestService.VERSION_REQUEST_URL;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * The ci spring profile ensures that integration test will execute against the in memory database
 */
@SpringApplicationConfiguration(ApiApplication.class)
@WebIntegrationTest
@ActiveProfiles({"ci"})
public class VersionRestServiceComponentTest extends ApiApplicationTest {

    @Test
    public void thatVersionResponseContainsApplicationVersion() throws Exception {
        ResponseEntity<String> response = getForEntity(VERSION_REQUEST_URL, String.class);
        String responseBody = response.getBody();
        assertThat(responseBody, containsString("applicationVersion"));
    }

    @Test
    public void thatVersionResponseContainsDatabaseVersion() throws Exception {
        ResponseEntity<String> response = getForEntity(VERSION_REQUEST_URL, String.class);
        String responseBody = response.getBody();
        assertThat(responseBody, containsString("databaseVersion"));
    }
}