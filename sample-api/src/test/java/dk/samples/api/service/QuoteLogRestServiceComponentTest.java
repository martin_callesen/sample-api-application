package dk.samples.api.service;

import dk.samples.ApiApplication;
import dk.samples.ApiApplicationTest;
import dk.samples.api.dto.QuoteLogRestRequest;
import dk.samples.api.dto.QuoteLogRestResponse;
import dk.samples.api.dto.QuoteLogSearchRestResponse;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static dk.samples.api.dto.QuoteLogSearchRestRequest.TODAY;
import static dk.samples.api.service.QuoteLogRestService.QUOTE_LOG_REQUEST_URL;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;


@SpringApplicationConfiguration(ApiApplication.class)
@WebIntegrationTest
@ActiveProfiles({"ci"})
public class QuoteLogRestServiceComponentTest extends ApiApplicationTest {

    @Test
    public void addQuoteLog() throws Exception {
        QuoteLogRestRequest request = QuoteLogRestRequest.create("a quote");
        Class entityClass = QuoteLogRestResponse.class;
        ResponseEntity<QuoteLogRestResponse> response = postForEntity(QUOTE_LOG_REQUEST_URL, request, entityClass);
        assertThat("QuoteLogRestResponse.statusCode", response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void searchForQuoteLogToday() throws Exception {
        String url = QUOTE_LOG_REQUEST_URL + "?period={period}";
        Class entityClass = QuoteLogSearchRestResponse.class;
        int period = TODAY.getPeriodeType();
        ResponseEntity<QuoteLogSearchRestResponse> response = getForEntity(url, entityClass, period);
        assertThat("QuoteLogSearchRestResponse.statusCode", response.getStatusCode(), equalTo(HttpStatus.OK));
    }
}
