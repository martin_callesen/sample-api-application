package dk.samples.api.service;

import dk.samples.ApiApplication;
import dk.samples.ApiApplicationTest;
import dk.samples.external.client.dto.ExternalQuoteRestResponse;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static dk.samples.api.service.QuoteRestService.QUOTE_REQUEST_URL;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * The ci spring profile ensures that integration test will execute against the in memory database
 */
@SpringApplicationConfiguration(ApiApplication.class)
@WebIntegrationTest
@ActiveProfiles({"ci"})
public class QuoteRestServiceComponentTest extends ApiApplicationTest {
    @Test
    public void doesServiceProvideAnAnswer() throws Exception {
        ResponseEntity<ExternalQuoteRestResponse> response = getForEntity(QUOTE_REQUEST_URL, ExternalQuoteRestResponse.class);
        Assert.assertThat("QuoteResponse.statusCode", response.getStatusCode(), equalTo(HttpStatus.OK));
    }
}