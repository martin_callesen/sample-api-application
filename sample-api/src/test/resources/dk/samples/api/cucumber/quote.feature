Feature: Quotes

  Background:
    Given quote "This is a quote"
    Given quote retrieved "today"

  Scenario: As an quote viewer, I want to have a quote, so my thought process will start.
    When I retrieve a quote
    Then quote should be "This is a quote"

  Scenario: As an quote provider,
            I want to see what quotes has been viewed,
            so I know areas of quotes I should improved.
    When I retrieve quotes viewed "today"
    Then I can see a quote has been viewed "today"