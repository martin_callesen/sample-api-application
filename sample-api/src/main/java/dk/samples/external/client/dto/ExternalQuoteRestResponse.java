package dk.samples.external.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalQuoteRestResponse {
    public static final String NO_QUOTE = "NO QUOTE AVAILABLE";
    public static final String QUOTE_AVAILABLE = "success";
    public static final Value NO_VALUE = new Value();
    public static final ExternalQuoteRestResponse NO_QUOTE_AVAILABLE = ExternalQuoteRestResponse.withQuote(NO_VALUE, NO_QUOTE);
    private String type;
    private Value value;

    public ExternalQuoteRestResponse() {
    }

    public static ExternalQuoteRestResponse withQuote(String quote, String type) {
        Value value = new Value(Value.NO_ID, quote);

        return withQuote(value, type);
    }

    private static ExternalQuoteRestResponse withQuote(Value value, String type) {
        ExternalQuoteRestResponse response = new ExternalQuoteRestResponse();
        response.value = value;
        response.type = type;

        return response;
    }

    public static String asString(ExternalQuoteRestResponse response) {
        String randomQuote = NO_QUOTE;

        if(hasQuote(response)){
            randomQuote = response.toString();
        }

        return randomQuote;
    }

    private static boolean hasQuote(ExternalQuoteRestResponse quoteResponse) {
        return quoteResponse != null;
    }

    public String getType() {
        return this.type;
    }

    public Value getValue() {
        return this.value;
    }


    @Override
    public String toString() {
        return "ExternalQuoteRestResponse{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }

    public static String getQuote(ExternalQuoteRestResponse response) {
        String quote = Value.NO_QUOTE;

        if(response != null && response.value != null){
            quote = response.value.getQuote();
        }

        return quote;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Value {
        public static final String NO_QUOTE = "no quote";
        public static final long NO_ID = -1;
        private Long id;
        private String quote;

        public Value() {
            this.id = NO_ID;
            this.quote = NO_QUOTE;
        }

        public Value(Long id, String quote) {
            this.id = id;
            this.quote = quote;
        }

        public Long getId() {
            return id;
        }

        public String getQuote() {
            return quote;
        }

        @Override
        public String toString() {
            return "Value{" +
                    "id=" + id +
                    ", quote='" + quote + '\'' +
                    '}';
        }
    }
}
