package dk.samples.external.client;

import dk.samples.external.client.dto.ExternalQuoteRestResponse;

public interface ExternalQuoteClient {
    ExternalQuoteRestResponse getQuote();
}
