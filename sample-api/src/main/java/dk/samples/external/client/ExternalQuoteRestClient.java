package dk.samples.external.client;

import dk.samples.external.client.dto.ExternalQuoteRestResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ExternalQuoteRestClient implements ExternalQuoteClient {
    private final RestTemplate restTemplate;

    @Value("${external.quote.service.url}")
    private String urlToService;

    public ExternalQuoteRestClient() {
        this.restTemplate = new RestTemplate();
    }

    @Override
    public ExternalQuoteRestResponse getQuote() {
        return restTemplate.getForObject(urlToService, ExternalQuoteRestResponse.class);
    }
}
