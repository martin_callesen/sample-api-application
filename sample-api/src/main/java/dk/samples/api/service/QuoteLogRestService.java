package dk.samples.api.service;

import dk.samples.domain.QuoteLog;
import dk.samples.domain.QuoteLogService;
import dk.samples.api.dto.QuoteLogRestRequest;
import dk.samples.api.dto.QuoteLogRestResponse;
import dk.samples.api.dto.QuoteLogSearchRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuoteLogRestService {
    public static final String QUOTE_LOG_REQUEST_URL = "/api/quotelog";

    @Autowired
    private QuoteLogService quoteLogService;

    @RequestMapping(value = QUOTE_LOG_REQUEST_URL, method = RequestMethod.POST)
    @ResponseBody
    QuoteLogRestResponse createQuoteLog(@RequestBody QuoteLogRestRequest quote) {
        String quoteContent = quote.getQuote();
        QuoteLog quoteLog = quoteLogService.createQuoteLog(quoteContent);
        QuoteLogRestResponse response = QuoteLogRestResponse.create(quoteLog.toString());

        return response;
    }

    @RequestMapping(value = QUOTE_LOG_REQUEST_URL, method = RequestMethod.GET)
    @ResponseBody
    QuoteLogSearchRestResponse getQuoteLog(@RequestParam int period) {
        Iterable<QuoteLog> quoteLogs = quoteLogService.searchForQuotes(period);
        QuoteLogSearchRestResponse resp = new QuoteLogSearchRestResponse();

        for (QuoteLog log : quoteLogs){
            String quoteLog = log.toString();
            resp.addQuoteLog(quoteLog);
        }

        return resp;
    }
}
