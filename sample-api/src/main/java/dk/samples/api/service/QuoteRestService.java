package dk.samples.api.service;

import dk.samples.api.dto.QuoteRestResponse;
import dk.samples.domain.QuoteLogService;
import dk.samples.external.client.ExternalQuoteClient;
import dk.samples.external.client.dto.ExternalQuoteRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuoteRestService {
    public static final String QUOTE_REQUEST_URL = "/api/quote";

    @Autowired
    private QuoteLogService quoteLogService;

    @Autowired
    private ExternalQuoteClient externalQuoteClient;

    @RequestMapping(QUOTE_REQUEST_URL)
    QuoteRestResponse randomQuote(){
        ExternalQuoteRestResponse response = this.externalQuoteClient.getQuote();
        String asString = ExternalQuoteRestResponse.asString(response);
        this.quoteLogService.createQuoteLog(asString);
        String quote = ExternalQuoteRestResponse.getQuote(response);

        return QuoteRestResponse.create(quote);
    }

}
