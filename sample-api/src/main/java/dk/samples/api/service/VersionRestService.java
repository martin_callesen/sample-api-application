package dk.samples.api.service;

import dk.samples.domain.Version;
import dk.samples.api.dto.VersionRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionRestService {
    public static final String VERSION_REQUEST_URL = "/api/version";

    @Autowired
    private Version version;

    @RequestMapping(VERSION_REQUEST_URL)
    @ResponseBody
    VersionRestResponse stateVersion() {
        String applicationVersion = version.getApplicationVersion();
        String databaseVersion = version.getDatabaseVersion();

        return VersionRestResponse.createVersionRestResponse(applicationVersion, databaseVersion);
    }
}
