package dk.samples.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteLogRestRequest {
    private String quote;

    public static QuoteLogRestRequest create(String quote) {
        QuoteLogRestRequest request = new QuoteLogRestRequest();
        request.quote = quote;

        return request;
    }

    public String getQuote() {
        return this.quote;
    }
}
