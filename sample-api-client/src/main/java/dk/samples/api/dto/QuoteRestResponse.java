package dk.samples.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteRestResponse {
    private String quote;

    public String getQuote() {
        return this.quote;
    }

    public static QuoteRestResponse create(String quote) {
        QuoteRestResponse response = new QuoteRestResponse();
        response.quote = quote;

        return response;
    }
}
