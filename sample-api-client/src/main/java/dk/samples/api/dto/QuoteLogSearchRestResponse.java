package dk.samples.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteLogSearchRestResponse {
    private List<QuoteLogRestResponse> responseList = new ArrayList<>();

    public void addQuoteLog(String log) {
        QuoteLogRestResponse logRestResponse = QuoteLogRestResponse.create(log);
        this.responseList.add(logRestResponse);
    }

    public boolean containsQuote(String externalQuote) {
        boolean quoteFound = false;

        for (QuoteLogRestResponse response : this.responseList){
            if(response.toString().contains(externalQuote)){
                quoteFound = true;
                break;
            }
        }

        return quoteFound;
    }
}
