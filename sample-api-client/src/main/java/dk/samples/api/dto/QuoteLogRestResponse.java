package dk.samples.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteLogRestResponse {
    private String quoteLog;

    public static QuoteLogRestResponse create(String quoteLog) {
        QuoteLogRestResponse response = new QuoteLogRestResponse();
        response.quoteLog = quoteLog;

        return response;
    }
}
