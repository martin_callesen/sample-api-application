package dk.samples.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionRestResponse {
    private String applicationVersion;
    private String databaseVersion;

    public static VersionRestResponse createVersionRestResponse(String applicationVersion, String databaseVersion) {
        VersionRestResponse versionRestResponse = new VersionRestResponse();
        versionRestResponse.applicationVersion = applicationVersion;
        versionRestResponse.databaseVersion = databaseVersion;

        return versionRestResponse;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getDatabaseVersion() {
        return databaseVersion;
    }
}
