package dk.samples.api.dto;

public enum QuoteLogSearchRestRequest {
    TODAY(1);
    private final int periodetype;

    QuoteLogSearchRestRequest(int type) {
        this.periodetype = type;
    }

    public int getPeriodeType() {
        return periodetype;
    }
}
