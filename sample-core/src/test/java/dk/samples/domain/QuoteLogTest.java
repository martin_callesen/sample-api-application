package dk.samples.domain;

import org.junit.Test;

import static dk.samples.domain.QuoteLogEntity.createQuoteLog;
import static dk.samples.time.ClockUtils.fixSystemDateTimeTo;
import static java.time.LocalDateTime.parse;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class QuoteLogTest {

    @Test
    public void quoteLogHasADate() throws Exception {
        fixSystemDateTimeTo("2015-10-25T10:15:30.00Z");
        QuoteLog quoteLog = createQuoteLog("<BLABLA>");
        assertThat("Date", parse("2015-10-25T11:15:30.00"), is(quoteLog.getDate()));
    }

    @Test
    public void isNotPersisted() throws Exception {
        QuoteLog quoteLog = createQuoteLog("<BLABLA>");
        assertFalse("QuoteLog", quoteLog.isPersisted());
    }
}