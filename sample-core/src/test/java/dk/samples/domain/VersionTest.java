package dk.samples.domain;

import dk.samples.database.DatabaseMigrator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VersionTest {
    private static final String IS_UNKNOWN = Version.UNKNOWN;

    @InjectMocks
    private Version version;

    @Mock
    private DatabaseMigrator migrator;

    @Test
    public void databaseVersionThrowsAnException() throws Exception {
        when(migrator.getDatabaseVersion()).thenThrow(new RuntimeException());
        String databaseVersion = version.getDatabaseVersion();
        assertThat("DatabaseVersion", databaseVersion, is(IS_UNKNOWN));
    }

    @Test
    public void databaseVersionIsOk() throws Exception {
        when(migrator.getDatabaseVersion()).thenReturn("123");
        String databaseVersion = version.getDatabaseVersion();
        assertThat("DatabaseVersion", databaseVersion, is("123"));
    }
}