package dk.samples.database;

public interface DatabaseMigrator {
    String getDatabaseVersion();
}
