package dk.samples.database;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.api.MigrationVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatabaseFlywayMigrator implements DatabaseMigrator {

    @Autowired
    public Flyway flyway;

    @Override
    public String getDatabaseVersion() {
        MigrationInfoService migrationInfoService = this.flyway.info();
        MigrationInfo migrationInfo = migrationInfoService.current();
        MigrationVersion migrationVersion = migrationInfo.getVersion();

        return migrationVersion.getVersion();
    }
}