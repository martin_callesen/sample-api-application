package dk.samples.time;

import java.time.Clock;
import java.time.LocalDateTime;

public class LocalDateTimeUtils {
    public static LocalDateTime now() {
        Clock clock = ClockUtils.getClock();
        return LocalDateTime.now(clock);
    }
}
