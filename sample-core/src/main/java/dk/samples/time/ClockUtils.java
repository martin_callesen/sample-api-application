package dk.samples.time;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class ClockUtils {
    private static Clock clock = Clock.systemDefaultZone();

    public static Clock getClock() {
        return clock;
    }

    public static void fixed(Instant instant, ZoneId zone) {
        clock = Clock.fixed(instant, zone);
    }

    public static void fixSystemDateTimeTo(String dateInString) {
        ZoneId zone = ZoneId.of("Europe/Paris");
        Instant instant = Instant.parse(dateInString);
        fixed(instant, zone);
    }
}
