package dk.samples.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class QuoteLogService {
    @Autowired
    private QuoteLogRepository quoteLogRepository;

    public QuoteLog createQuoteLog(String quote){
        QuoteLogEntity savedLog = QuoteLogEntity.NO_LOG;

        if(!StringUtils.isEmpty(quote)){
            QuoteLogEntity quoteLog = QuoteLogEntity.createQuoteLog(quote);
            savedLog = quoteLogRepository.save(quoteLog);
        }

        return savedLog;
    }

    public Iterable<QuoteLog> searchForQuotes(int period) {
        Iterable logEntities = quoteLogRepository.findAll();
        
        return logEntities;
    }
}
