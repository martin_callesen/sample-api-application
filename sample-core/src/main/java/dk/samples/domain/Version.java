package dk.samples.domain;

import dk.samples.database.DatabaseMigrator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Service
public class Version {
    private final Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private DatabaseMigrator databaseMigrator;
    public static final String UNKNOWN = "UNKNOWN";

    public String getDatabaseVersion() {
        String databaseVersion = UNKNOWN;

        try {
            databaseVersion = this.databaseMigrator.getDatabaseVersion();
        } catch (Exception e) {
            log.warn("Could not get database version", e);
        }

        return databaseVersion;
    }

    public String getApplicationVersion() {
        String version = UNKNOWN;
        Class<? extends Version> aClass = this.getClass();
        Package aPackage = aClass.getPackage();
        String implementationVersion = aPackage.getImplementationVersion();

        if (!StringUtils.isEmpty(implementationVersion)) {
            version = implementationVersion;
        }

        return version;
    }
}
