package dk.samples.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface QuoteLogRepository extends CrudRepository<QuoteLogEntity, Long> {
}
