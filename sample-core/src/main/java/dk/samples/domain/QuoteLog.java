package dk.samples.domain;

import java.time.LocalDateTime;

public interface QuoteLog {
    LocalDateTime getDate();
    boolean isPersisted();
}
