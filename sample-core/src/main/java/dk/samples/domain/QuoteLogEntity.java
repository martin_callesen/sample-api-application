package dk.samples.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static dk.samples.time.LocalDateTimeUtils.now;

@Entity
@Table(name = "quote_log")
class QuoteLogEntity implements QuoteLog {
    public static final QuoteLogEntity NO_LOG = new QuoteLogEntity();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private LocalDateTime date;

    @NotNull
    private String quoteResponse;

    @Override
    public LocalDateTime getDate() {
        return this.date;
    }

    public static QuoteLogEntity createQuoteLog(String response) {
        QuoteLogEntity quoteLogEntity = new QuoteLogEntity();
        quoteLogEntity.date = now();
        quoteLogEntity.quoteResponse = response;

        return quoteLogEntity;
    }

    @Override
    public boolean isPersisted() {
        return this.id > 0;
    }

    @Override
    public String toString() {
        return "QuoteLog{" +
                "id=" + id +
                ", date=" + date +
                ", quoteResponse='" + quoteResponse + '\'' +
                '}';
    }
}
