# Java development environment with docker #
Show an example on a development environment for java running an docker container with a mysql database and
a docker container for the application server. Inside the application server a spring boot application communicates with an external service on the internet.

### How do I get set up? ###

You must install a docker and docker-compose on your machine https://www.docker.com

Then open a terminal navigate to you project folder and do docker-compose -f build.yml up and then docker-compose -f docker-compose.dev.yml up

Expect it to take about 10 minutes on the first go.

### How do I access the application ###
Open a browser and navigation to http://192.168.33.10:8080/api/version or http://192.168.33.10:8080/api/quote